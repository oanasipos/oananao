# Personal website

In order to see your site in action, run Hugo's built-in local server.

    $ hugo server -w

Now enter `localhost:1313` in the address bar of your browser.
